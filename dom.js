// js code for loader
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}




let dataUrl = "https://restcountries.com/v3.1/all";
fetchdata()

function fetchdata() {
    fetch(dataUrl)
        .then((data) => data.json())
        .then((data) => {
            let count = 0;
            return data.reduce((accumulator, currentvalue) => {
                let flag = currentvalue.flags.png;
                let country = currentvalue.name.official;
                let population = currentvalue.population;
                let region = currentvalue.region;
                let capital = currentvalue.capital;

                accumulator[count] = {};
                accumulator[count]["flag"] = flag;
                accumulator[count]["country"] = country;
                accumulator[count]["population"] = population;
                accumulator[count]["region"] = region;
                accumulator[count]["capital"] = capital;
                count++;
                return accumulator;
            }, []);
        })
        .then((data) => {
            setTimeout(() => {
                countryDisplay(data);
                return data
            }, 2000)

        })
        .catch((err) => {
            console.log(err)
            alert("some error in data file")
        })
}




function countryDisplay(data) {

    // creating heading
    const body = document.body;
    const headingDiv = document.createElement("div")
    const heading = document.createElement("h1");
    heading.innerText = "where in the world";
    headingDiv.appendChild(heading)
    body.insertBefore(headingDiv, document.getElementById("flex-container"));
    body.style.backgroundColor = "#F5F5DC"
    headingDiv.style.display = "flex";
    headingDiv.style.marginBottom = "4vh"





    let searchAndFilterDiv = document.createElement("div")
    let searchCountryBar = document.createElement("label")
    let selecter = document.createElement("select")
    searchCountryBar.innerText = "filter by region : "
    let option0 = document.createElement("option");
    option0.innerText = "All regions"
    selecter.appendChild(option0)
    let option1 = document.createElement("option");
    option1.innerText = "Africa"
    selecter.appendChild(option1)
    let option2 = document.createElement("option")
    option2.textContent = "Asia"
    selecter.appendChild(option2)
    let option3 = document.createElement("option")
    option3.textContent = "Americas"
    selecter.appendChild(option3)
    let option4 = document.createElement("option")
    option4.textContent = "Europe"
    selecter.appendChild(option4)
    let option5 = document.createElement("option")
    option5.textContent = "Oceania"
    selecter.appendChild(option5)


    searchCountryBar.appendChild(selecter)
    searchAndFilterDiv.appendChild(searchCountryBar)
    body.insertBefore(searchAndFilterDiv, document.getElementById("flex-container"));


    // search and filter div styling

    searchAndFilterDiv.style.display = "flex";
    searchAndFilterDiv.style.margin = "10vh"
    searchCountryBar.style.marginLeft = "auto"








    let section = document.getElementById("flex-container");

    //styling the container
    section.style.display = "flex";
    section.style.flexWrap = "wrap";
    section.style.rowGap = "10vh";
    section.style.columnGap = "4.5vw";
    section.style.padding = "0vh 7vw ovh 7vw";
    section.style.marginLeft = "5vw";
    section.style.marginRight = "5vw";


    //filtering div 

    const h1 = document.querySelector("select");

    h1.addEventListener("click", function (e) {
        let clickedRegion = e.target.value;
        console.log(clickedRegion)
        let filteredArr = data.filter((country) => {
            if (country.region == clickedRegion) {
                return country
            }
        })
        console.log(filteredArr)
        let a1 = document.querySelectorAll(".country-div")
        a1.forEach((ele)=>{
            ele.remove()
        })
        filteredArr.forEach(country => {
            let newdiv = document.createElement("div");  //creating div for each individual country
            newdiv.className = "country-div";
            newdiv.style.width = "21%";
            newdiv.style.backgroundColor = "white";              // styling individual country
            section.appendChild(newdiv);

            let countryImage = document.createElement("img");  // creating image of flag
            countryImage.style.height = "12rem";
            countryImage.style.width = "100%";
            countryImage.className = "flag";
            countryImage.style.marginBottom = "0.7rem";
            countryImage.setAttribute("src", country.flag);
            newdiv.appendChild(countryImage);

            let countryNameDiv = document.createElement("div");   //creating div  for country
            countryNameDiv.className = "country-name";
            countryNameDiv.innerText = country.country;
            countryNameDiv.style.fontSize = "1.5rem";
            countryNameDiv.style.fontWeight = "600";
            countryNameDiv.style.marginBottom = "1.5rem";
            countryNameDiv.style.marginLeft = "0.8rem"

            newdiv.appendChild(countryNameDiv);

            let countryCapitalDiv = document.createElement("div");    //creating div for country-capital
            countryCapitalDiv.className = "country-capital";
            countryCapitalDiv.innerText = `capital : ${country.capital}`;
            countryCapitalDiv.style.marginLeft = "0.8rem"
            newdiv.appendChild(countryCapitalDiv);


            let countryRegionDiv = document.createElement("div");    //creating div for country-region
            countryRegionDiv.className = "country-Region";
            countryRegionDiv.innerText = `Region : ${country.region}`;
            countryRegionDiv.style.marginLeft = "0.8rem"
            newdiv.appendChild(countryRegionDiv);

            let countryPopulationDiv = document.createElement("div");     //creating div for country-population
            countryPopulationDiv.className = "country-Population";
            countryPopulationDiv.innerText = `population : ${country.population}`;
            countryPopulationDiv.style.marginLeft = "0.8rem"
            newdiv.appendChild(countryPopulationDiv);

        });





       
    })



    for (let country of data) {

        let newdiv = document.createElement("div");  //creating div for each individual country
        newdiv.className = "country-div";
        newdiv.style.width = "21%";
        newdiv.style.backgroundColor = "white";            // styling individual country
        section.appendChild(newdiv);


        let countryImage = document.createElement("img");  // creating image of flag
        countryImage.style.height = "12rem";
        countryImage.style.width = "100%";
        countryImage.className = "flag";
        countryImage.style.marginBottom = "0.7rem";
        countryImage.setAttribute("src", country.flag);
        newdiv.appendChild(countryImage);

        let countryNameDiv = document.createElement("div");   //creating div  for country
        countryNameDiv.className = "country-name";
        countryNameDiv.innerText = country.country;
        countryNameDiv.style.fontSize = "1.5rem";
        countryNameDiv.style.fontWeight = "600";
        countryNameDiv.style.marginBottom = "1.5rem";
        countryNameDiv.style.marginLeft = "0.8rem"

        newdiv.appendChild(countryNameDiv);

        let countryCapitalDiv = document.createElement("div");    //creating div for country-capital
        countryCapitalDiv.className = "country-capital";
        countryCapitalDiv.innerText = `capital : ${country.capital}`;
        countryCapitalDiv.style.marginLeft = "0.8rem"
        newdiv.appendChild(countryCapitalDiv);


        let countryRegionDiv = document.createElement("div");    //creating div for country-region
        countryRegionDiv.className = "country-Region";
        countryRegionDiv.innerText = `Region : ${country.region}`;
        countryRegionDiv.style.marginLeft = "0.8rem"
        newdiv.appendChild(countryRegionDiv);

        let countryPopulationDiv = document.createElement("div");     //creating div for country-population
        countryPopulationDiv.className = "country-Population";
        countryPopulationDiv.innerText = `population : ${country.population}`;
        countryPopulationDiv.style.marginLeft = "0.8rem"
        newdiv.appendChild(countryPopulationDiv);



    }

    const h2 = document.querySelector(".country-div");

    h2.addEventListener("click", function (e) {
        //window.open("./popup.html")
        console.log(e.target)
        console.log(e.target.src);
        console.log(e.target.value)
        console.log("image click")
        //individualdisplay(e.target.src)

    })




}




